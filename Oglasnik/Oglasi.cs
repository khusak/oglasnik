﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oglasnik {
    class Oglasi {
        public Oglasi() {
            oglasi = new List<Oglas>();
            slikovniOglasi = new List<SlikovniOglas>();
            videoOglasi = new List<VideoOglas>();
        }

        public void DodajOglas(Oglas oglas) {
            oglasi.Add(oglas);
        }

        public void DodajSlikovniOglas(SlikovniOglas oglas) {
            slikovniOglasi.Add(oglas);
        }
        public void DodajVideoOglas(VideoOglas oglas) {
            videoOglasi.Add(oglas);
        }

        public List<Oglas> SviOglasi { get => oglasi; }

        private List<Oglas> oglasi;
        private List<SlikovniOglas> slikovniOglasi;
        private List<VideoOglas> videoOglasi;
    }
}

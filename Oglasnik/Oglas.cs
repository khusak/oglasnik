﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oglasnik {
    class Oglas {
        public Oglas(string naslov, string opis, string autor = "") {
            Naslov = naslov;
            Opis = opis;
            Autor = autor;
            DatumObjave = DateTime.Now;
        }

        public override string ToString() {
            return string.Format("{0} {1}\n", Naslov, Opis);
        }

        public string Naslov { get; set; }
        public string Opis { get; set; }
        public string Autor { get; set; }
        public DateTime DatumObjave { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Oglasnik {
    public partial class Form1 : Form {
        Oglasi oglasi;

        public Form1() {
            InitializeComponent();
            oglasi = new Oglasi();
        }

        private void Form1_Load(object sender, EventArgs e) {
        }

        private void btnAddAd_Click(object sender, EventArgs e) {
            oglasi.DodajOglas(new Oglas(tbTitle.Text, tbContent.Text));
            PrikaziOglase();
        }

        private void PrikaziOglase() {
            tbAds.Text = "";
            foreach (var oglas in oglasi.SviOglasi) {
                tbAds.Text += oglas.ToString();
            }
        }
    }
}

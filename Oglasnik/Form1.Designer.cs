﻿
namespace Oglasnik {
    partial class Form1 {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.lblTitle = new System.Windows.Forms.Label();
            this.lblContent = new System.Windows.Forms.Label();
            this.tbTitle = new System.Windows.Forms.TextBox();
            this.tbContent = new System.Windows.Forms.TextBox();
            this.cbPicture = new System.Windows.Forms.CheckBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.cbVideo = new System.Windows.Forms.CheckBox();
            this.btnAddAd = new System.Windows.Forms.Button();
            this.tbAds = new System.Windows.Forms.TextBox();
            this.lblAds = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Location = new System.Drawing.Point(34, 28);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(83, 15);
            this.lblTitle.TabIndex = 0;
            this.lblTitle.Text = "Naslov oglasa:";
            // 
            // lblContent
            // 
            this.lblContent.AutoSize = true;
            this.lblContent.Location = new System.Drawing.Point(34, 99);
            this.lblContent.Name = "lblContent";
            this.lblContent.Size = new System.Drawing.Size(73, 15);
            this.lblContent.TabIndex = 1;
            this.lblContent.Text = "Tekst oglasa:";
            // 
            // tbTitle
            // 
            this.tbTitle.Location = new System.Drawing.Point(34, 59);
            this.tbTitle.Name = "tbTitle";
            this.tbTitle.Size = new System.Drawing.Size(220, 23);
            this.tbTitle.TabIndex = 2;
            // 
            // tbContent
            // 
            this.tbContent.Location = new System.Drawing.Point(34, 132);
            this.tbContent.Multiline = true;
            this.tbContent.Name = "tbContent";
            this.tbContent.Size = new System.Drawing.Size(220, 174);
            this.tbContent.TabIndex = 3;
            // 
            // cbPicture
            // 
            this.cbPicture.AutoSize = true;
            this.cbPicture.Location = new System.Drawing.Point(34, 334);
            this.cbPicture.Name = "cbPicture";
            this.cbPicture.Size = new System.Drawing.Size(101, 19);
            this.cbPicture.TabIndex = 4;
            this.cbPicture.Text = "Slikovni oglas:";
            this.cbPicture.UseVisualStyleBackColor = true;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(34, 372);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(220, 23);
            this.textBox3.TabIndex = 5;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(34, 456);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(220, 23);
            this.textBox4.TabIndex = 7;
            // 
            // cbVideo
            // 
            this.cbVideo.AutoSize = true;
            this.cbVideo.Location = new System.Drawing.Point(34, 418);
            this.cbVideo.Name = "cbVideo";
            this.cbVideo.Size = new System.Drawing.Size(90, 19);
            this.cbVideo.TabIndex = 6;
            this.cbVideo.Text = "Video oglas:";
            this.cbVideo.UseVisualStyleBackColor = true;
            // 
            // btnAddAd
            // 
            this.btnAddAd.Location = new System.Drawing.Point(34, 512);
            this.btnAddAd.Name = "btnAddAd";
            this.btnAddAd.Size = new System.Drawing.Size(220, 43);
            this.btnAddAd.TabIndex = 8;
            this.btnAddAd.Text = "Dodaj oglas:";
            this.btnAddAd.UseVisualStyleBackColor = true;
            this.btnAddAd.Click += new System.EventHandler(this.btnAddAd_Click);
            // 
            // tbAds
            // 
            this.tbAds.Location = new System.Drawing.Point(290, 59);
            this.tbAds.Multiline = true;
            this.tbAds.Name = "tbAds";
            this.tbAds.Size = new System.Drawing.Size(360, 496);
            this.tbAds.TabIndex = 9;
            // 
            // lblAds
            // 
            this.lblAds.AutoSize = true;
            this.lblAds.Location = new System.Drawing.Point(290, 28);
            this.lblAds.Name = "lblAds";
            this.lblAds.Size = new System.Drawing.Size(43, 15);
            this.lblAds.TabIndex = 10;
            this.lblAds.Text = "Oglasi:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(696, 597);
            this.Controls.Add(this.lblAds);
            this.Controls.Add(this.tbAds);
            this.Controls.Add(this.btnAddAd);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.cbVideo);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.cbPicture);
            this.Controls.Add(this.tbContent);
            this.Controls.Add(this.tbTitle);
            this.Controls.Add(this.lblContent);
            this.Controls.Add(this.lblTitle);
            this.Name = "Form1";
            this.Text = "Oglasnik";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Label lblContent;
        private System.Windows.Forms.TextBox tbTitle;
        private System.Windows.Forms.TextBox tbContent;
        private System.Windows.Forms.CheckBox cbPicture;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.CheckBox cbVideo;
        private System.Windows.Forms.Button btnAddAd;
        private System.Windows.Forms.TextBox tbAds;
        private System.Windows.Forms.Label lblAds;
    }
}

